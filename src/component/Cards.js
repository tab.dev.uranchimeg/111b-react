import React, {useState} from 'react';
import { data } from './data';
import './style.css'
function Cards() {
    const [largeData , setLargeData] = useState(data)
    const [hide, setHide] = useState(true);
     const deletePress = (key) => {
           const newArr = largeData.filter((item) => item?.key!==key)
           setLargeData(newArr)
     }
    return ( 
        <div>
            {
                largeData &&  largeData.map((item, index) => {
                    return(
                        <div 
                        key={item?.key}
                        className="imgContainer">
                             <img src={item?.img} alt="Loading..." />
                             <div className="huwaalt">
                                  <p>{item?.littleTitle}</p>
                                  <p>{item?.price}</p>
                             </div>
                             <div>
                                 {
                                    hide ? <p>{item?.text.substring(0, item?.maxLength)}...</p>
                                    :
                                    <p>{item?.text}</p>
                                 } 
                                 {
                                    hide ? <p onClick={()=> setHide(false)}>Read more</p> :  <p onClick={() => setHide(true)}>Read less</p>
                                 } 
                             </div>
                             <div className='buttonContainer'>
                                 <button onClick={() =>deletePress(item?.key)}>Delete</button>
                             </div>
                             
                        </div>
                    )
                })
            }
        </div>
     );
}

export default Cards;