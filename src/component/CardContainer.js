import React from 'react';
import Cards from './Cards';
import './style.css';
function CardContainer() {
    return (  
        <div className='container'>
            <h1 className='text'>Our Tours</h1>
            <Cards/>
        </div>
    );
}

export default CardContainer;